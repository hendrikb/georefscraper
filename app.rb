# frozen_string_literal: true
require 'rubygems'
require 'logging'
require 'yaml'
require 'redis'
require 'sequel'

ENV['ENV'] = 'development' unless ENV['ENV']

require_relative './lib/log_helper'
require_relative './lib/social_network'
require_relative './lib/enrichment'

CONFIG = YAML.load_file(File.join('config', 'app.yml'))[ENV['ENV']]

Logging.logger.root.appenders = [
  Logging.appenders.stdout(layout: Logging::Layouts::Pattern.new),
  Logging.appenders.file(File.join('logs', "georefscraper_#{ENV['ENV']}.log"),
                         layout: Logging::Layouts::Pattern.new)
]

ll = if !!CONFIG['log_level'] then CONFIG['log_level'].to_sym else :info end
Logging.logger.root.level = ll

CACHE = Redis.new(CONFIG['cache']['redis'])

DB = Sequel.connect(CONFIG['persistence'])
