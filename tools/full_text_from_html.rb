require 'nokogiri'
require 'socket'

def stanford_ner(plain_text_document)
  begin
    client = TCPSocket.open('localhost', 8080)
  rescue Errno::ECONNREFUSED => e
    raise e, 'Please make sure the ner_service.sh is running.'
  end
  client.puts(plain_text_document.gsub(/\s+/, ' '))
  ner_response = ''
  while line = client.gets
    ner_response += line
  end
  client.close_read
  ner_response
end

Dir.glob(File.join('..', 'data', 'dumps', '*.html')).each do |file|
  # file = '../data/dumps/Carl_Civella.html'
  html = Nokogiri::HTML(IO.read file)
  heading = html.at('h1#firstHeading').inner_text
  puts heading

  content = html.at('div#mw-content-text')
  content.xpath("//span[@id='External_links']/parent::h2/following-sibling::*").remove
  content.xpath("//span[@id='External_links']/parent::h2").remove
  puts stanford_ner(content.inner_text)
end
