require_relative File.join('..', 'app')

CACHE.keys.each do |k|
  next if k.match('index.php') || CACHE.get(k).length < 1000 || !k.match(/^http/)
  f = File.open(File.join('data', 'dumps', "#{k.split('/').last}.html"), 'w')
  f.write(CACHE.get(k))
  f.close
end
