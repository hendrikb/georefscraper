# frozen_string_literal: true
require 'spec_helper'

describe SocialNetwork::Converter::Cypher do
  let(:me) { SocialNetwork::Person.new('n1', 'TNp', 'Hendrik') }
  let(:mum) { SocialNetwork::Person.new('n2', 'TNp', 'Mum') }
  let(:dad) { SocialNetwork::Person.new('n3', 'TNp', 'Dad') }

  let(:relationship_married) do
    SocialNetwork::Relationship.new(mum, dad, 'married')
  end
  let(:relationship_f_son) { SocialNetwork::Relationship.new(dad, me, 'son') }
  let(:relationship_m_son) { SocialNetwork::Relationship.new(mum, me, 'son') }

  let(:network) do
    actor_list = SocialNetwork::Helper::ActorList.new([me, mum, dad])
    relationship_list = SocialNetwork::Helper::RelationshipList.new(
      [relationship_married, relationship_f_son, relationship_m_son])

    SocialNetwork::Base.new('The "Test" Family', actor_list, relationship_list)
  end

  context 'conversion' do
    it 'responds to module method convert' do
      expect(SocialNetwork::Converter::Cypher).to respond_to(:convert)
        .with(2).arguments
    end
    it 'converts stuff to cy' do
      skip 'needs some work on proper generic hashing of relation labels'
      ref_cy = <<EOF
// Cypher for neo4j, Graph: The \"Test\" Family
CREATE (n1:Actor:Person:TNp {name:'Hendrik', type:'TNp'})
CREATE (n2:Actor:Person:TNp {name:'Mum', type:'TNp'})
CREATE (n3:Actor:Person:TNp {name:'Dad', type:'TNp'})
CREATE
\t(n3)-[:SOCIALNETWORK%3A%3ARELATIONSHIP+%5BN2%5D--MARRIED--%5BN3%5D]->(n3),
\t(n3)-[:SOCIALNETWORK%3A%3ARELATIONSHIP+%5BN2%5D--SON--%5BN1%5D]->(n1),
\t(n2)-[:SOCIALNETWORK%3A%3ARELATIONSHIP+%5BN3%5D--SON--%5BN1%5D]->(n1)
EOF
      expect(SocialNetwork::Converter::Cypher.convert(network)).to eq ref_cy
    end
  end
end
