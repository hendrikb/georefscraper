# frozen_string_literal: true
require 'spec_helper'

module Enrichment
  describe RequestHttpRedirectNeededError do
    let(:example_url) { 'http://example.com/foo/bar' }
    subject { described_class.new example_url }
    it { should respond_to(:message) }
    it 'gives url back as location' do
      expect(subject.location).to eq example_url
    end
  end
  describe RequestHttpNotFoundError do
    it { should respond_to(:message) }
  end
  describe RequestPermanentlyFailedError do
    subject { described_class.new 'Something went wrong' }
    it { should respond_to(:message) }
  end
end
