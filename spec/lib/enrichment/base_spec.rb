# frozen_string_literal: true
require 'spec_helper'

describe Enrichment::Base do
  let(:social_network) do
    graphml_file = File.new(File.join('spec', 'test_ref_tiny.graphml'))
    SocialNetwork::Parser::GraphML.parse(graphml_file)
  end

  subject { Enrichment::Base.new(social_network) }
  it { should respond_to(:enrich!) }
  it 'fails if no social_network is given' do
    expect { Enrichment::Base.new(nil) }
      .to raise_error Enrichment::InvalidSocialNetworkError
  end
end
