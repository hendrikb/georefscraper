# frozen_string_literal: true
require 'spec_helper'

describe Enrichment::Scrape::Relation do
  let(:left) { 'George Bush' }
  let(:type) { :lives_in }
  let(:right) { 'Austin, TX' }
  let(:result) do
    not_sure = Enrichment::Search::Result::CONFIDENCE_NOT_SURE
    Enrichment::Search::Result.new('http://example.com',
                                   '<html>some html</html>',
                                   not_sure,
                                   Enrichment::Search::Provider::Wikipedia)
  end
  subject { described_class.new(left, type, right, result) }
  context 'initialization' do
    it 'expects three arguments' do
      expect(described_class).to respond_to(:new).with(4).arguments
    end
    it 'assigns left' do
      expect(subject.left).to eq left
    end
    it 'assigns type' do
      expect(subject.type).to eq type
    end
    it 'assigns right' do
      expect(subject.right).to eq right
    end
    it 'assigns right' do
      expect(subject.result).to eq result
    end
  end
  context 'human readability' do
    it 'reads out a good name' do
      expect(subject.to_s).to eq 'George Bush lives in Austin, TX [stated in http://example.com]'
    end
  end
end
