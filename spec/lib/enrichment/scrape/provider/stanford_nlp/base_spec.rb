# frozen_string_literal: true
require 'spec_helper'

describe Enrichment::Scrape::Provider::StanfordNLP::Base do
  context '.scrape' do
    let(:search_result) do
      Enrichment::Search::Result.new(
        'https://en.wikipedia.org/wiki/George_H._W._Bush',
        File.read(File.join('spec', 'enrichment_george_h_w_bush.html')),
        Enrichment::Search::Result::CONFIDENCE_SOMEWHAT_SURE,
        Enrichment::Search::Provider::Wikipedia
      )
    end
    it 'returns an array' do
      expect(subject.scrape(search_result)).to be_kind_of(Array)
    end

    context 'with a valid search_result' do
    end
  end
end
