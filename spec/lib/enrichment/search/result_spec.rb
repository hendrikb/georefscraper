# frozen_string_literal: true
require 'spec_helper'

describe Enrichment::Search::Result do
  let(:url) { 'http://example.com/foo/bar?baz' }
  let(:html) { '<html><head><title>hello world</title></head></html>' }
  let(:conf_level) { Enrichment::Search::Result::CONFIDENCE_NOT_SURE }
  let(:search_provider_origin) { Enrichment::Search::Provider::Wikipedia::Base }
  subject { described_class.new(url, html, conf_level, search_provider_origin) }
  context 'initializing' do
    it 'expects four parameters' do
      expect(described_class).to respond_to(:new).with(4).arguments
    end
    it 'assigns the resource' do
      expect(described_class.new(url, html, conf_level, search_provider_origin).resource).to eq url
    end
    it 'assigns the resource' do
      expect(described_class.new(url, html, conf_level, search_provider_origin).html).to eq html
    end
    it 'assigns the confidence' do
      expect(described_class.new(url, html, conf_level, search_provider_origin).confidence)
        .to eq conf_level
    end
    it 'assigns the search provider origin' do
      expect(described_class.new(url, html, conf_level, search_provider_origin).search_provider_origin)
        .to eq search_provider_origin
    end
    it 'fails if nil resource was given' do
      expect { described_class.new(nil, html, 4, search_provider_origin) }
        .to raise_error Enrichment::Search::MissingResourceError
    end
    it 'fails if nil html source code was given' do
      expect { described_class.new(url, nil, 4, search_provider_origin) }
        .to raise_error Enrichment::Search::MissingHtmlError
    end
    it 'fails if blank html source code was given' do
      expect { described_class.new(url, '', 4, search_provider_origin) }
        .to raise_error Enrichment::Search::MissingHtmlError
    end
    [nil, -1].each do |invalid_value|
      it "fails if invalid (#{invalid_value}) confidence level was given" do
        expect { described_class.new(url, html, invalid_value, search_provider_origin) }
          .to raise_error Enrichment::Search::InvalidConfidenceValueError
      end
    end
  end
  context 'comparing' do
    it 'returns true if url and confidence level match' do
      expect(subject).to eq described_class.new(url, html, conf_level, search_provider_origin)
    end
  end
end
