# frozen_string_literal: true
require 'spec_helper'

describe Enrichment::Search::Provider::Base do
  # We basically don't have any testable logic here so far. Everything will
  # be in the sub classes
  # it {should respond_to(:search).with(1).argument}
  context 'initialize' do
    let(:social_network) do
      graphml_file = File.new(File.join('spec', 'test_ref_tiny.graphml'))
      SocialNetwork::Parser::GraphML.parse(graphml_file)
    end

    it 'assigns a social_network' do
      subject = Enrichment::Search::Provider::Base.new(social_network)
      expect(subject.instance_variable_get(:@social_network))
        .to eq(social_network)
    end
  end
end
