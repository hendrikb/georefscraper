# frozen_string_literal: true
require 'spec_helper'

describe Enrichment::Search::Provider::Wikipedia::DisambiguationHelper do
  let(:html) { File.read('spec/enrichment_bush.html') }
  let(:social_network) { SocialNetwork::Base.new('Bush') }
  subject { described_class.new(social_network) }

  context 'classification' do
    it 'classifies George Bush page as a disambiguation page' do
      html = File.read('spec/enrichment_bush.html')
      expect(Enrichment::Search::Provider::Wikipedia::DisambiguationHelper
        .disambiguation_page?(html)).to be_truthy
    end
  end
  context 'parsing' do
    it { should respond_to(:parse_html_for).with(2).arguments }
    it 'returns empty array if something faulty given' do
      expect(subject.parse_html_for(nil, nil)).to eq []
    end
    context 'disambiguation site parsing' do
      subject do
        described_class.new(social_network)
      end

      it 'returns 2 links from disambiguation page at max' do
        first_actor = social_network.actors.first
        expect(subject.parse_html_for(html, first_actor)).to eq [
          'https://en.wikipedia.org/wiki/George_H._W._Bush']
      end
    end
  end
end
