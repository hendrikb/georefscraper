require 'spec_helper'

describe Enrichment::Search::Provider::Wikipedia::SearchHelper do
  let(:social_network) { SocialNetwork::Base.new('Bush') }
  let(:some_html) { '<html><head></head><body>Some HTML!</body></html>' }
  let(:wikipedia) { Enrichment::Search::Provider::Wikipedia }
  subject { described_class.new(social_network) }

  context 'parsing' do
    it 'returns empty if Wikipedia Search gave no results' do
      doc = Nokogiri::HTML(
        '<html><body>There were no results matching the query.</body></html>')
      expect(subject.check_results(doc, nil)).to eq []
    end
    it 'returns empty if search results are simply not given on the page' do
      doc = Nokogiri::HTML('<html><body>Nothing here...</body></html>')
      expect(subject.check_results(doc, nil)).to eq []
    end

    context 'real result pages' do
      it 'hits parse_results' do
        expect(subject).to receive(:parse_results).and_return ['blar']
        doc = Nokogiri::HTML(File.read('spec/wikipedia_search_bcci_bush.html'))
        expect(subject.check_results(doc, nil)).to eq ['blar']
      end
      context 'return values based on confidence' do
        let(:doc) do
          Nokogiri::HTML(File.read('spec/wikipedia_search_bcci_bush.html'))
        end
        it 'returns PROB_WRONG result on Wikipedia Listing' do
          res1 = Enrichment::Search::Result.new(
            'https://en.wikipedia.org/wiki/Safari_Club', some_html,
            Enrichment::Search::Result::CONFIDENCE_NOT_SURE, wikipedia)

          expect(subject.check_results(doc, nil)
            .map { |r| [r.resource, r.confidence] })
            .to eq [[res1.resource, res1.confidence]]
        end
        it 'returns SOMEWHAT_SURE if neighbors appear in description' do
          res1 = Enrichment::Search::Result.new(
            'https://en.wikipedia.org/wiki/Safari_Club', some_html,
            Enrichment::Search::Result::CONFIDENCE_SOMEWHAT_SURE, wikipedia)

          kerry = SocialNetwork::Person.new('p1', 'jk', 'John Kerry')
          safari = SocialNetwork::Organization.new('o1', 'sc', 'Safari Club')
          rel = SocialNetwork::Relationship.new(kerry, safari)

          al = SocialNetwork::Helper::ActorList.new([kerry, safari])
          rl = SocialNetwork::Helper::RelationshipList.new([rel])

          network = SocialNetwork::Base.new('n1', al, rl)
          subject = described_class.new(network)
          expect(subject.check_results(doc, kerry)
            .map { |r| [r.resource, r.confidence] })
            .to eq [[res1.resource, res1.confidence]]
        end
        it 'returns SOMEWHAT_SHURE if actorlabel appears in link description' do
          pending 'implement me'
          raise
        end
      end
      context 'confidence estimation' do
      end
    end
  end
end
