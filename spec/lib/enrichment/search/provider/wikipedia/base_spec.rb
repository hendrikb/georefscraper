# frozen_string_literal: true
require 'spec_helper'

describe Enrichment::Search::Provider::Wikipedia::Base do
  context 'initialization' do
    let(:article_url) { 'http://example.com/wiki/something' }
    before do
      allow(subject).to receive(:article_url_for).and_return article_url
    end
    let(:social_network) do
      graphml_file = File.new(File.join('spec', 'test_ref_tiny.graphml'))
      SocialNetwork::Parser::GraphML.parse(graphml_file)
    end

    subject do
      described_class.new(social_network)
    end

    it { expect(described_class).to respond_to(:new).with(2).arguments }

    it 'assigns @disambiguation_helper internally' do
      expect(subject.instance_variable_get(:@disambiguation_helper))
        .to be_a_kind_of(
          Enrichment::Search::Provider::Wikipedia::DisambiguationHelper)
    end

    it 'raises when tried to be initialized without any parameters' do
      expect { described_class.new.to raise_error NoMethodError }
    end
    context '#search' do
      it 'returns array when request fails permanently' do
        pending 'There is a bug in direct_article_html that makes it search and THAT returns [[]]'
        allow(subject).to receive(:get_html_for)
          .and_raise(Enrichment::RequestPermanentlyFailedError.new('test'))
        expect(subject.search(social_network.actors.first)).to eq []
      end

      it 'returns array when request fails with 404' do
        pending 'There is a bug in direct_article_html that makes it search and THAT returns [[]]'
        allow(subject).to receive(:get_html_for)
          .and_raise(Enrichment::RequestHttpNotFoundError)
        expect(subject.search(social_network.actors.first)).to eq []
      end

      it 'returns to search_wikipedia if request fails after redirect' do
        allow(subject).to receive(:get_html_for).and_return 'default' * 100
        allow(subject).to receive(:get_html_for).once.with(article_url)
          .and_raise(Enrichment::RequestHttpRedirectNeededError
          .new('somewhere-else'))
        allow(subject).to receive(:get_html_for).once.with('somewhere-else')
          .and_raise('redirect failes a second time')
        expect(subject).to receive(:search_wikipedia).once
          .with(social_network.actors.first).and_return(-1)
        expect(subject.search(social_network.actors.first)).to eq(-1)
      end

      it 'returns single article url array if html is plain article hit' do
        search_actor = social_network.actors.first
        expect_any_instance_of(Enrichment::Search::Provider::Base).to receive(:get_html_for)
          .and_return('Something that looks like long html code' * 100)
        expect(Enrichment::Search::Provider::Wikipedia::Base)
          .to receive(:plain_article?).once.and_return true
        result = Enrichment::Search::Result
                 .new(article_url, File.read('spec/enrichment_george_h_w_bush.html'),
                      Enrichment::Search::Result::CONFIDENCE_SOMEWHAT_SURE,
                      Enrichment::Search::Provider::Wikipedia)
        expect(subject.search(search_actor)).to eq [result]
      end

      it 'hit search_wikipedia if we have no idea what the HTML is' do
        search_actor = social_network.actors.first
        expect_any_instance_of(Enrichment::Search::Provider::Base).to receive(:get_html_for)
          .and_return 'something too short to be HTML code...'
        expect(subject).to receive(:search_wikipedia).and_return []
        expect(subject.search(search_actor)).to eq []
      end
    end

    context 'classification' do
      it 'classifies fritz reuter page as a plain article' do
        html = File.read('spec/enrichment_reuter.html')
        expect(Enrichment::Search::Provider::Wikipedia::Base
          .plain_article?(html)).to be_truthy
      end
    end
  end
end
