# frozen_string_literal: true
require 'spec_helper'

describe Enrichment::Search::Provider::Wikipedia do
  context '.build_wiki_url' do
    it 'returns a wikipedia URL' do
      expect(described_class.build_wiki_url('test'))
        .to eq 'https://en.wikipedia.org/test'
    end
  end
end
