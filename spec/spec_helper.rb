# frozen_string_literal: true
ENV['ENV'] = 'test'

require_relative File.join('..', 'app')
require 'rspec/logging_helper'
require 'simplecov'

RSpec.configure do |config|
  include RSpec::LoggingHelper
  config.capture_log_messages
end

SimpleCov.start do
  add_filter '/spec/'
end
