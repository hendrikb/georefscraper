# GEOREFSCRAPER

Analyze and import social networks (works with Lombardi networks:
http://www.lombardinetworks.net/networks/the-networks/) and geo-locate their
actors.

## Installation

### Prerequesite: Installation of Java 1.8


Stanford CoreNLP is written in Java; current releases require Java 1.8+.
Therefor make sure you install Java 1.8 on your machine.

For Ubuntu 14.04 unfortunately you have to go to:
http://www.oracle.com/technetwork/java/javase/downloads/index.html

And install it manually.

**Alternatively** you CAN allow a PPA on ubuntu 14.04, this is not recommended, but
works:

```
sudo -s
echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
apt-get update
apt-get install oracle-java8-installer
```
Please exit afterwards.

Check that java 1.8 works by checking: ```java -version```

### Installation of the backend


The following command order is tested on a ubuntu 14.04 LTS Server. It is assumed that you follow all these commands from your $HOME directory, but should work from any other as well:

1. Import RVMs private key:  ```gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3```
2. Install Ruby Version Manager: ```\curl -sSL https://get.rvm.io | bash -s stable```
3. Log out and in to make installation take effect
3. Install Ruby: ```rvm install 2.3.0```
4. Get georefscraper source codes: ```wget https://gitlab.com/hendrikb/georefscraper/repository/archive.zip?ref=master```
5. Install necessary applications: ```sudo apt-get install unzip git bash-completion mysql-server mysql-client libmysqlclient-dev redis-server```
6. (You will be asked for a mysql root password - remember it - in this example it will be **TESTME**)
7. Unzip the archive: ```unzip master.zip```
8. Change into the prototype directory: ```cd georefscraper-*```
9. Install Ruby libraries: ```gem install bundler nokogiri && bundle install```
10. Edit ```config/app.yml```  -- see "production:" branch and set your mysql credentials there. user: **root**, pass: **TESTME**
11. Create the production database itself: ```echo 'CREATE DATABASE georef_prod' | mysql -u root -pTESTME```
12. Create the production database tables: ```ENV=production rake database:create_schema```

## General Usage

You are now completely set up. You can run ```rake -t``` see all available tasks.

**REMEMBER**: You have to prepend every command with ```ENV=production``` since you are using this in a productional environment in this tutorial. If you leave out ```ENV=production``` the rake application assumes you are using ```ENV=development```. You can do that, but yoy have to set up the database accordingly.

## Example run: Start enriching

**Hint**: You can scrape all lombardi networks from www.lombardi-networks.com by calling ```./tools/get_lombardi_networks.sh```

You now have lots of lombardi networks in your working directory. Start enriching one!

```ENV=production rake lombardi_graphml:enrich[HarkenBush5th.graphml]```

This will take a lot of time, so please make sure you have a cup of coffee!

**Note**: You can cancel the process at any time via CTRL-C. All states are cached using the redis service, so restarting/repeating this task will go quicker.

## Frontend/API
This project is a command line tool. For visualization, APIs and a frontend, go to the frontend part of this prototype:

FRONTEND / API: https://gitlab.com/hendrikb/georef_frontend_api/ 

## Tools

* We have a ```./tools/get_lombardi_networks.sh``` shell script that automatically crawls
the latest digitalized Lombardi Networks ("Narrative Structures") from
http://www.lombardinetworks.net/
* Play around with the ```rake -t``` command to see what tasks are available.
* Hint: Check out the ```rake lombardi_graphml:convert_to:dot[HarkenBush5th.graphml,HARKENBUSH]``` command to create a DOT file that can be visualized using graphviz
