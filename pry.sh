#!/bin/sh

pry -v >/dev/null 2>&1 || { echo >&2 "Please install the bundle here: gem install bundler && bundle install"; exit 1; }

pry -r ./app.rb
