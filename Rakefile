require_relative File.join('.', 'app')

require 'yard'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'

namespace :lombardi_graphml do
  desc 'Enrich lombardi network'
  task :enrich, [:graphml] do |_t, args|
    sn = SocialNetwork::Parser::GraphML.parse(File.open(args[:graphml]))
    Enrichment::Base.new(sn).enrich!
  end
  namespace :merge_to do
    desc "Merge graphml files (space sep't) by their actor labels to dot graph"
    task :dot, [:graphmls] do |_t, args|
      require 'digest'

      result_actors = SocialNetwork::Helper::ActorList.new([])
      result_relationships = SocialNetwork::Helper::RelationshipList.new([])
      args[:graphmls].split(' ').each do |graphml|
        sn = SocialNetwork::Parser::GraphML.parse(File.new(graphml))
        sn.actors.each do |actor|
          actor.id = Digest::SHA256.base64digest(actor.label)
          begin
            result_actors << actor
          rescue SocialNetwork::Helper::DuplicateActorError
            $stderr.puts "// #{graphml}: duplicate actor #{actor}"
          end
        end

        sn.relationships.each do |rl|
          begin
            result_relationships << rl
          rescue SocialNetwork::Helper::DuplicateRelationshipError
            $stderr.puts "// #{graphml}: duplicate relationship #{rl}"
          end
        end
      end
      results = SocialNetwork::Base.new('Results', result_actors,
                                        result_relationships)
      puts SocialNetwork::Converter::Dot.convert(results)
    end
    desc "Merge graphml files (space sep't) by their actor labels to cypher"
    task :cypher, [:graphmls] do |_t, args|
      # TODO: heavy code duplication for merge_to cypher//dot
      require 'digest'

      result_actors = SocialNetwork::Helper::ActorList.new([])
      result_relationships = SocialNetwork::Helper::RelationshipList.new([])
      args[:graphmls].split(' ').each do |graphml|
        sn = SocialNetwork::Parser::GraphML.parse(File.new(graphml))
        sn.actors.each do |actor|
          actor.id = Digest::SHA256.base64digest(actor.label)
          begin
            result_actors << actor
          rescue SocialNetwork::Helper::DuplicateActorError
            $stderr.puts "// #{graphml}: duplicate actor #{actor}"
          end
        end

        sn.relationships.each do |rl|
          begin
            result_relationships << rl
          rescue SocialNetwork::Helper::DuplicateRelationshipError
            $stderr.puts "// #{graphml}: duplicate relationship #{rl}"
          end
        end
      end
      results = SocialNetwork::Base.new('Results', result_actors,
                                        result_relationships)
      puts SocialNetwork::Converter::Cypher.convert(results)
    end
  end

  desc 'Validate social network lombardi graphml. Look for exceptions in files!'
  task :validate, [:graphml, :name] do |_t, args|
    begin
      print "Validating #{args[:graphml]} ... "
      SocialNetwork::Parser::GraphML.parse(File.new(args[:graphml]))
    rescue StandardError => e
      $stderr.puts "\nThe graphml #{args[:graphml]} couldn't be validated"
      $stderr.puts "The error while building up the structure was:\n\t#{e}"
      exit 1
    end
    puts 'OK'
  end
  namespace :convert_to do
    desc 'Convert lombardi GraphML representation to DOT graph file format'
    task :dot, [:graphml, :name] do |_t, args|
      social_network = SocialNetwork::Parser::GraphML.parse(
        File.new(args[:graphml]), network_name: args[:name])

      puts SocialNetwork::Converter::Dot.convert(social_network)
    end

    desc 'Convert lombardi GraphML representation to Cypher format (for neo4j)'
    task :cypher, [:graphml, :name] do |_t, args|
      social_network = SocialNetwork::Parser::GraphML.parse(
        File.new(args[:graphml]), network_name: args[:name])

      puts SocialNetwork::Converter::Cypher.convert(social_network)
    end

    desc 'Print out all Actors from GraphML file'
    task :actor_list, [:graphml] do |_t, args|
      social_network = SocialNetwork::Parser::GraphML.parse(
        File.new(args[:graphml]), ommit_relationships: true)
      puts SocialNetwork::Converter::ActorList.convert(social_network)
    end
  end
end

namespace :database do
  desc '(Dump and Re-)Create database structure (CAUTION!)'
  task :create_schema do
    # DB.drop_table?(:search_results, :entity_mentions, :relations, :relationships, :actors, :social_networks)
    DB << 'SET FOREIGN_KEY_CHECKS = 0' << 'DROP TABLES IF EXISTS entity_mentions, search_results, relations, relationships, actors, social_networks;'
    DB.create_table :social_networks do
      primary_key :id
      String :name
      DateTime :created_at
    end

    DB.create_table :actors do
      primary_key :id
      String :node_id
      String :label
      String :class
      String :type
      foreign_key :social_network_id, :social_networks, on_delete: :cascade
      DateTime :created_at
    end

    DB.create_table :relationships do
      primary_key :id
      foreign_key :source, :actors, on_delete: :cascade
      foreign_key :target, :actors, on_delete: :cascade
      foreign_key :social_network_id, :social_networks, on_delete: :cascade
      String :type
      DateTime :created_at
    end

    DB.create_table :entity_mentions do
      primary_key :id
      Fixnum :corefID
      Fixnum :eend
      Fixnum :estart
      Fixnum :headPosition
      Fixnum :hend
      Fixnum :hstart
      String :objectId
      String :type
      String :value
      DateTime :created_at
    end

    DB.create_table :search_results do
      primary_key :id
      String :resource
      Fixnum :confidence
      String :html, text: true
      String :search_provider_origin
      foreign_key :actor_id, :actors, on_delete: :cascade
      DateTime :created_at
    end

    DB.create_table :relations do
      primary_key :id
      foreign_key :actor_id, :actors, on_delete: :cascade
      foreign_key :left, :entity_mentions, on_delete: :cascade
      foreign_key :right, :entity_mentions, on_delete: :cascade
      String :type
      foreign_key :search_result_id, :search_results, on_delete: :cascade
      DateTime :created_at
    end
  end
end

RSpec::Core::RakeTask.new(:spec)

YARD::Rake::YardocTask.new do |t|
  t.name    = 'doc:create'
  t.files   = ['lib/**/*.rb']
  t.stats_options = ['--list-undoc']
  t.after = proc do
    puts "\nIf you see undocumented parts of code, feel free to help out. :)"
  end
end

desc 'Show all available rake tasks by default.'
task :default do
  system 'rake --tasks'
end

RuboCop::RakeTask.new
