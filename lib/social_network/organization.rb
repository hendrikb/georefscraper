# frozen_string_literal: true
module SocialNetwork
  # A specialization of the {Actor} class - use this class once you know that
  # an actor in a social network is an organization
  class Organization < Actor
  end
end
