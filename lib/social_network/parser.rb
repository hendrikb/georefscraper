# frozen_string_literal: true
require_relative './parser/graphml'
module SocialNetwork
  # Holds classes, that are able to parse data structures (such as GraphML)
  # into SocialNetwork::Base classes.
  module Parser
  end
end
