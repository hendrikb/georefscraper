# frozen_string_literal: true
require_relative './converter/dot'
require_relative './converter/cypher'
require_relative './converter/actor_list'

module SocialNetwork
  # Holds classes, that are able to convert social networks to other formats
  # (e.g. DOT file format, plain text actor lists, Cypher (neo4j), etc)
  module Converter
  end
end
