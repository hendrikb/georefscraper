# frozen_string_literal: true
module SocialNetwork
  # Represents some kind of actor in a social network
  class Actor
    attr_accessor :id, :type, :search_results, :relations
    attr_reader :label

    # @param id [Object] A unique id within one social network. i.e. a String
    # @param type [Object] Some identifier for this type of actor, i.e. a String
    # @param label [String] A human readble name for this {Actor}
    def initialize(id, type, label)
      if id.nil? || id.empty?
        raise ActorIdInvalidError, 'Actor id must not be empty nor nil'
      end

      @id = id.freeze
      @type = type
      @label = sanitize(label)
      @search_results = []
      @relations = []
    end

    # Tells whether this instance of a {Actor} is equal to another. Equality is
    # defined by equality of {Actor#id their ids} only!
    # @param other [Actor] The other actor to compare this instance against
    # @return [Boolean] True if both {Actor#id actor ids} are the same.
    def ==(other)
      id == other.id
    end

    # Provide a human readable name for a {Actor} in a social network
    # @return [String] Human readable name
    def inspect
      "#{self.class}[#{id}] \"#{label}\""
    end

    # Sets the {Actor#label}, i.e. human readable representation of this Actor
    # @param label [String] New human readable label to be assigned
    # @return [void]
    def label=(label)
      @label = sanitize(label)
    end

    alias to_s inspect

    # Tells whether is some other {Actor} is valid by looking at its class or
    # superclass. Either of them must be {Actor}
    # @param other {Object} The object, preferably an {Actor}, to check
    # @return [Boolean] True if is a valid instance of actor or any subclass
    def self.valid?(other)
      other.class == Actor || other.class.superclass == Actor
    end

    private

    def sanitize(label)
      SocialNetwork::Helper::ActorLabelSanitizer.sanitize(label)
    end
  end

  # This Exceptio gets raised if you've tried to initialize a {Actor} with a nil
  # or empty {Actor#id}
  class ActorIdInvalidError < StandardError; end
end
