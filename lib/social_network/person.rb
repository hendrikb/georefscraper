# frozen_string_literal: true
module SocialNetwork
  # A specialization of the {Actor} class - use this class once you know that
  # an actor in a social network is a person
  class Person < Actor
  end
end
