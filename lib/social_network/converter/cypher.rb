# frozen_string_literal: true
require 'cgi'

module SocialNetwork
  # Holds classes, that are able to convert social networks to other formats
  # (e.g. Cypher file format)
  module Converter
    # Module providing static method #convert to convert social networks to
    # {http://www.graphviz.org/ graphviz'} Cypher file format
    module Cypher
      # Class providing functionality to convert social networks to GraphML.
      # It'srecommended to not use it directly. Use
      # #convert instead
      class Converter
        # param social_network [SocialNetwork::Base] a social network to convert
        def initialize(social_network)
          @sn = social_network
        end

        # Runs the convert process towards a Cypher file output
        # @return [String} A string representation of a social network in Cypher
        def convert
          @cy = "// Cypher for neo4j, Graph: #{@sn.name}\n"
          write_actors
          @cy << "CREATE\n" unless @sn.empty?
          write_relationships
          @cy
        end

        private

        def write_actors
          @sn.actors.each do |actor|
            @cy << "CREATE (#{actor.id.delete('=')}:Actor:" \
              "#{labelize_class_of(actor)}:#{labelize_type_of(actor)} " \
              "#{attributes_for(actor)})\n"
          end
        end

        def write_relationships
          @sn.relationships.each do |relationship|
            @cy << "\t(#{relationship.source.id})-[:" \
              "#{relationship_type(relationship)}]->(#{relationship.target.id})"
            @cy << ',' unless relationship == @sn.relationships.last
            @cy << "\n"
          end
        end

        def labelize_class_of(actor)
          actor.class.to_s.split(':').last
        end

        def labelize_type_of(thing)
          CGI.escape(thing.to_s)
        end

        def attributes_for(actor)
          "{name:'#{actor.label.delete("'")}', " \
            "type:'#{actor.type.to_s.delete("'")}'}"
        end

        def relationship_type(relationship)
          return 'UNSPECIFIED_RELATION' unless relationship.type
          labelize_type_of(relationship).upcase
        end
      end

      # Converts the given social network to the Cypher file format
      # @param social_network [SocialNetwork::Base] Network to convert to Cypher
      # @param _options [Hash] Currently unused options
      # @return [String] A string representation of the Social network in Cypher
      def self.convert(social_network, _options = {})
        Converter.new(social_network).convert
      end
    end
  end
end
