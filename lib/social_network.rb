# frozen_string_literal: true
require_relative './social_network/base'

require_relative './social_network/actor'
require_relative './social_network/organization'
require_relative './social_network/person'
require_relative './social_network/relationship'

require_relative './social_network/converter'
require_relative './social_network/helper'
require_relative './social_network/parser'

# Holds a couple of functional aspects of a social network
module SocialNetwork
end
