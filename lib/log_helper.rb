# frozen_string_literal: true
# Minor Module to provide small logging helper functionalities
module LogHelper
  # Shortens a long class name (with modules and all) into something shorter
  # @param class_name [Class] The class which name is shortened
  # @return [String] SometHing::Very::Long becomes S:V:Long
  def self.prefix(class_name)
    cn = class_name.to_s
    identifiers = cn.split('::')
    identifiers.map! { |i| i.gsub(/[a-z]+/, '') }
    identifiers[identifiers.length - 1] = cn.split('::').last
    "(#{ENV['ENV']})#{identifiers.join(':')}"
  end
end
