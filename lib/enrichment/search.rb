# frozen_string_literal: true
require_relative './search/provider'
require_relative './search/result'

module Enrichment
  # Module namespace for search related logics. SEARCH in these terms means
  # looking up resources (mainly: URLs), that can be scraped for actor
  # information
  module Search
  end
end
