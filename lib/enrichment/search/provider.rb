# frozen_string_literal: true
require_relative './provider/base'
require_relative './provider/wikipedia'

module Enrichment
  module Search
    # Module to namespace Search Provider classes
    module Provider
    end
  end
end
