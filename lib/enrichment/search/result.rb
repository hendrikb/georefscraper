# frozen_string_literal: true
module Enrichment
  module Search
    # Represents the result of a search class  under
    # {Enrichment::Search::Provider}. In most cases this just means, that a URL
    # is given as resource and a confidence_level is guestimated as a hint how
    # sure we were if the found resource actually represents what we were
    # actually looking for.
    class Result
      # Pointed out result IS definitely a representation for what we looked for
      CONFIDENCE_DEFINITELY_SURE  = 4
      # We are kind of sure that the pointed  out result might be a
      # representation for what we looked for
      CONFIDENCE_SOMEWHAT_SURE    = 3
      # Doubts exist, that the pointed out resource represents what we were
      # looking for
      CONFIDENCE_NOT_SURE         = 2
      # Major doubts exist, we are pretty sure this does not represent what we
      # were looking for.
      CONFIDENCE_PROBABLY_WRONG   = 1
      # We KNOW that this resource points to the wrong artefact.
      CONFIDENCE_DEFINITELY_WRONG = 0

      attr_reader :resource, :html, :confidence, :search_provider_origin
      # Produces an instance of this class. Raises {MissingResourceError} if
      # given resource was nil and raises [InvalidConfidenceValueError] if
      # given confidence level was not valid (see parameter list below).
      # @param resource [Object] Most likely a URL to a search result
      # @param html [String] The resource's HTML source code for scraping
      # @param confidence [Fixnum] Representation of how sure we are that the
      #   resource field points to what we were looking for. Either the value
      #   of CONFIDENCE_DEFINITELY_WRONG, CONFIDENCE_PROBABLY_WRONG,
      #   CONFIDENCE_NOT_SURE, CONFIDENCE_SOMEWHAT_SURE or
      #   CONFIDENCE_DEFINITELY_SURE
      # @param search_provider_origin [Class] Class name of the
      #   {Enrichment::Search::Provider}} that gave this result
      def initialize(resource, html, confidence, search_provider_origin)
        raise MissingResourceError if resource.nil?
        raise MissingHtmlError if html.nil? || html.empty?
        raise InvalidConfidenceValueError unless valid_confidrence?(confidence)
        @resource = resource
        @html = html
        @confidence = confidence
        @search_provider_origin = search_provider_origin
      end

      # Tells whether the other instance of this class and this instance are
      # eqal. Equality is defined by telling whether confidence_level and url
      # match
      # @param other [Enrichment::Search::Result] search result to tell if equal
      # @return [Boolean] True if confidence level and resource match with other
      def ==(other)
        other.resource == @resource && other.confidence == confidence
      end

      private

      def valid_confidrence?(level)
        [CONFIDENCE_DEFINITELY_WRONG, CONFIDENCE_PROBABLY_WRONG,
         CONFIDENCE_NOT_SURE, CONFIDENCE_SOMEWHAT_SURE,
         CONFIDENCE_DEFINITELY_SURE].include?(level)
      end
    end
    # Raised if someone forgot to provide a valid resource (i.e. a URL)
    class MissingResourceError < StandardError; end

    # Raised if someone forgot to provide source codes for a resource (i.e.HTML)
    class MissingHtmlError < StandardError; end

    # Given Confidence level for {Enrichment::Search::Result} was invalid
    class InvalidConfidenceValueError < StandardError; end
  end
end
