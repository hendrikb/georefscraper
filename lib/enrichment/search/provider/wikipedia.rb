# frozen_string_literal: true
require_relative './wikipedia/base'
require_relative './wikipedia/disambiguation_helper'
require_relative './wikipedia/search_helper'

module Enrichment
  module Search
    module Provider
      # Module that holds generic and specific logics to deal with English
      # wikipedia sites.
      module Wikipedia
        # Returns a human readable name for this provider, mainly used in
        # persistence phase
        def self.name
          'wikipedia_en'
        end

        # Build a valid Wikipedia URL if you know the anchor's HREF path from
        # a valid wikipedia page (i.e. from a link on a wikipedia page)
        # @return [String] A URL to a wikipedia article
        # @param path [String] A proper URL encoded path towards a wiki
        #   article beginning with "/wiki/" (e.g. "/wiki/Berlin")
        def self.build_wiki_url(path)
          return "https://en.wikipedia.org#{path}" if path.start_with?('/')
          "https://en.wikipedia.org/#{path}"
        end
      end
    end
  end
end
