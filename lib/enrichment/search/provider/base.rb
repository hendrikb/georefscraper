# frozen_string_literal: true
module Enrichment
  module Search
    module Provider
      # Amount of seconds to slow down (sleep) after making a web request
      SLOWING_DOWN_SECONDS = 2
      # Defines maximum number of returned resources per provider and actor
      SEARCH_RESULTS_MAXIMUM = 1
      # HTTP user agent to present to the outside world
      HTTP_USER_AGENT = 'georefscraper/search'.freeze

      # HTTP header fields to use to make request, some gzip stuff is important
      HTTP_HEADERS = {
        'User-Agent' => 'Mozilla/5.0',
        'accept-encoding' => 'gzip;q=0,deflate,sdch',
        'accept' => 'text/html'
      }.freeze

      # Provides logics to look for one web page (or whatever) that contains
      # information for actors on social networks. This class is mainly
      # subclassed by real search providers, like
      # {Enrichment::Search::Provider::Wikipedia::Base}.
      class Base
        attr_reader :log

        # Returns an instance of a Provider. Do not instantiate this directly,
        # use subclass or so.
        # @param social_network [SocialNetwork::Base] Optional social network
        #  that in some provider classes can be used to provide BETTER resources
        def initialize(social_network = nil)
          @log = Logging.logger[LogHelper.prefix(self.class.to_s)]
          log.debug "Initializing Enrichment Search Provider: #{self.class}"
          @social_network = social_network
        end

        private

        def get_html_for(url)
          log.info "Fetching contents for #{url}"
          if CACHE.exists url
            process_cached_url(url)
          else
            process_uncached_url(url)
          end
        end

        def process_cached_url(url)
          log.debug "CACHE HIT for #{url}"
          content = CACHE.get url
          if content =~ /\A30[0-9]/
            process_redirect_request(content, url)
          else
            fail_permanently(url, content) if content =~ /\A[0-9]{3}/
            content
          end
        end

        def fail_permanently(url, content)
          log.error "Can't get contents for #{url}, HTTP status: #{content}"
          raise RequestPermanentlyFailedError,
                "Request terminally failed for #{url} - Status: #{content}"
        end

        def process_redirect_request(store_string, url)
          redirect_url = store_string.split(';')[1]
          if url == redirect_url
            log.error "UH! Looks like #{url} redirected us to #{redirect_url}"
            raise RequestPermanentlyFailedError, 'Preventing cycle'
          end
          get_html_for(redirect_url)
        end

        def process_uncached_url(url)
          log.debug "CACHE MISS for #{url} - slowing down requesting"
          sleep SLOWING_DOWN_SECONDS

          response = HTTP[Enrichment::Search::Provider::HTTP_HEADERS].get(url)
          process_repsonse(response, url)
        end

        def process_repsonse(response, url)
          if (200...205).cover? response.status
            process_ok_response(response, url)
          else
            process_non_ok_response(response, url)
          end
        end

        def process_ok_response(response, url)
          CACHE.set url, response.to_s
          response.to_s
        end

        def process_non_ok_response(response, url)
          case response.status
          when 300...305
            process_300ish_response(response, url)
          when 404
            process_404_response(response, url)
          else
            process_other_non_ok_response(response, url)
          end
        end

        def process_300ish_response(response, url)
          log.warn "S:#{url}:#{response.status};#{response['Location']}"
          CACHE.set url, "#{response.status};#{response['Location']}"
          raise Enrichment::RequestHttpRedirectNeededError, response['Location']
        end

        def process_404_response(response, url)
          log.warn "S:#{url}:#{response.status}"
          CACHE.set url, response.status.to_s
          raise Enrichment::RequestHttpNotFoundError, "Not Found (404: #{url}"
        end

        def process_other_non_ok_response(response, url)
          log.warn "S:#{url}:#{response.status};DEADBEEF" # :)
          CACHE.set url, response.status.to_s
          raise RequestPermanentlyFailedError, "request failed for #{url}"
        end
      end
    end
  end
end
