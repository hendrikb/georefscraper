# frozen_string_literal: true
module Enrichment
  module Search
    module Provider
      module Wikipedia
        # Helper class to make parsing of English Wikipedia Disambiguation
        # Sites easier
        class DisambiguationHelper
          def initialize(social_network)
            @log = Logging.logger[LogHelper.prefix(self.class.to_s)]
            @social_network = social_network
          end

          # Parses HTML code of an English Wikipedia Disambigation page and
          # returns links that correspond well enough to a given actor
          # @param html [String] a HTML snipped as returned from wikipedia
          # @param actor [SocialNetwork::Actor::] Actor to look the
          #   Disambigation page for
          def parse_html_for(html, actor)
            @log.info "#{actor} page was a wikipedia disambiguation page."
            matches = []
            wiki_disambig_link_xpath = '//div[@id="mw-content-text"]/ul' \
                                       '/li/a[contains(@href,"/wiki")]'
            Nokogiri::HTML(html).xpath(wiki_disambig_link_xpath).each do |a|
              url = Enrichment::Search::Provider::Wikipedia
                    .build_wiki_url(a.attr('href'))
              # TODO: qualifies_as_disambig_match? must be changed to
              # estimated_disambig_match_confidence like below w/ searchresults
              matches << url if qualifies_as_disambig_match?(a, actor)
              break if matches.length == SEARCH_RESULTS_MAXIMUM
            end
            @log.info "Disambiguation p. parse for #{actor} returned #{matches}"
            matches
          end

          # Tells whether the given HTML is a disambiguation page at the english
          # wikipedia
          # @param html [String] the html to look at
          # @return [Boolean] trueish if HTML looks like a disambiguation page
          def self.disambiguation_page?(html)
            content = Nokogiri::HTML(html).at('//div[@id="mw-content-text"]')
            return false unless content
            content.inner_text.match(/disambiguation page/)
          end

          private

          def qualifies_as_disambig_match?(_html_anchor, _actor)
            # TODO: Implement me! @social_network neighbors? metrics?
            true
          end
        end
      end
    end
  end
end
