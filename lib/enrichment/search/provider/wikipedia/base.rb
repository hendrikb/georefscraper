# frozen_string_literal: true
require 'http'
require 'nokogiri'

module Enrichment
  module Search
    module Provider
      module Wikipedia
        # Lowest number of characters that need to be present before a web
        # request response is considered to be somewhat of html source code
        MINIMUM_HTML_CHARS = 500

        # English Wikipedia Search URL
        WIKI_SEARCH = 'https://en.wikipedia.org/w/' \
            'index.php?title=Special:Search&search=%s'.freeze

        # Search provider that searches resources from the English Wikipedia
        class Base < Enrichment::Search::Provider::Base
          def initialize(social_network = nil)
            @disambiguation_helper = DisambiguationHelper.new(social_network)
            @search_helper = SearchHelper.new(social_network)
            super(social_network)
          end
          # TODO: Everything must actually use the Enrichment::Search::Result
          # interface
          #
          # HTTP user agent, that this module uses - TODO: does it, actually?
          HTTP_USER_AGENT = 'georefscraper/search/wikipedia'.freeze

          # Lowest number of characters that need to be present in an
          # {SocialNetwork::Actor actor's} label before it is entered into the
          # Wikipedia search form, does not really make sense to look for
          # something like "ABC", since the chance is minimal to find something
          # we are actually looking for
          MIN_ACTOR_LABEL_LENGTH = 6

          # Searches for URLs (web resources) on the English Wikipedia for the
          # given {SocialNetwork::Actor}.
          # @param actor [SocialNetwork::Actor] The actor we want Web resources
          #     to be looked up for
          # @return [Array<String>] Array of URLs that are hits for the given
          #     actor on the English Wikipedia
          def search(actor)
            override_url = nil
            begin
              html = get_html_for(article_url_for(actor))
            rescue Enrichment::RequestPermanentlyFailedError,
                   Enrichment::RequestHttpNotFoundError
              html = '' # we have found nothing, let the algorithm continue
            rescue Enrichment::RequestHttpRedirectNeededError => e
              log.warn "Got redirect to #{e.location} while accessing direct URL #{article_url_for(actor)} - setting override URL"
              begin
                html = get_html_for(e.location)
                override_url = e.location
              rescue StandardError => e2
                log.error "Panic! Even redirect to #{e.location} failed with #{e2.message} - will just skip towards wiki search. :("
                html = ''
              end
            end

            unless too_short?(html)
              if self.class.plain_article?(html)
                return only_article_url_for(actor, override_url)
              elsif DisambiguationHelper.disambiguation_page?(html)
                return @disambiguation_helper.parse_html_for(html, actor)
              end
            end
            # nothing of the above worked as a direct approach, we need to
            # search actively using wikipedia's search engine
            search_wikipedia(actor)
          end

          # Tells whether the given HTML is a plain english wikipedia article
          # @param html [String] the html to look at
          # @return [Boolean] trueish if HTML looks like a wikipedia article
          def self.plain_article?(html)
            return false if html.nil? || html.to_s.length < MINIMUM_HTML_CHARS
            # TODO: The following line is evil, somewhere allow an array to
            # come in (might be from the search algorithm), we only want
            # strings here
            html = html.flatten.first.html if html.class == Array
            doc = Nokogiri::HTML(html).at('//div[@id="mw-content-text"]/p')
            return false unless doc
            doc.inner_text.match(/(was|is) an?/)
          end

          private

          def too_short?(html)
            return false if html && html.to_s.length > MINIMUM_HTML_CHARS
            @log.warn 'HTML is too short or nil here'
            true
          end

          def only_article_url_for(actor, override_url = nil)
            log.info "Nice! Wikipedia Page for #{actor} was a direct match"
            # TODO: Improve  Redis cache layer / Code Dupl in Wiki::SearcHHelper
            url = if override_url then override_url else article_url_for(actor) end
            if CACHE.exists url
              html = CACHE.get url
            else
              html = HTTP.get(url).to_s
              CACHE.set url, html
            end
            [Result.new(url, html,
                        Enrichment::Search::Result::CONFIDENCE_SOMEWHAT_SURE,
                        Enrichment::Search::Provider::Wikipedia)]
          end

          def article_url_for(actor)
            l = SocialNetwork::Helper::ActorLabelSanitizer.sanitize(actor.label)
            log.warn "Will use sanitized #{l} as URL base for #{actor.label}" if l != actor.label
            format 'https://en.wikipedia.org/wiki/%s', uri_encoded(l)
          end

          def enforce_actor_label_minimum_length(actor)
            l = SocialNetwork::Helper::ActorLabelSanitizer.sanitize(actor.label)
            raise 'Label is too short' if l.length < MIN_ACTOR_LABEL_LENGTH
          end

          def search_wikipedia(actor)
            l = SocialNetwork::Helper::ActorLabelSanitizer.sanitize(actor.label)
            log.warn "Will use sanitized \"#{l}\" as search base for #{actor.label}" if l != actor.label
            enforce_actor_label_minimum_length(actor)
            # TODO: Cleaner HTTP request with params: {...}
            html = get_html_for(
              format(WIKI_SEARCH, URI.encode(l.gsub('&', '%26'))))
          rescue Enrichment::RequestHttpRedirectNeededError => e
            log.info "Awesome! Searching for #{actor} returned direct match"
            return [e.location]
          rescue Enrichment::RequestHttpNotFoundError,
                 Enrichment::RequestPermanentlyFailedError, StandardError => e
            log.error("Search for #{actor} failed, " + e.message)
            return []
          ensure
            doc = Nokogiri::HTML(html)
            return [@search_helper.check_results(doc, actor)]
          end

          def uri_encoded(actor_label)
            URI.encode(actor_label.tr(' ', '_')).gsub('&', '%26')
          end
        end
      end
    end
  end
end
