# frozen_string_literal: true
require 'http'

module Enrichment
  module Search
    module Provider
      module Wikipedia
        # Instance of this class helps to parse English Wikipedia Search pages
        # for results
        class SearchHelper
          def initialize(social_network)
            @log = Logging.logger[LogHelper.prefix(self.class.to_s)]
            @social_network = social_network
          end

          # Parse search results from a Wikipedia Search HTML page
          # @param doc [Document] HTML of a English Wikipedia Search Result Page
          # @param actor [SocialNetwork::Actor] Actor to look up information for
          # @return [Array<Enrichment::Search::Result>] List of matching search
          #     results
          def check_results(doc, actor)
            return [] if there_were_no_results_in?(doc, actor)
            parse_results(doc.css('div.searchresults > ul > li'), actor)
          end

          private

          def there_were_no_results_in?(doc, actor)
            if doc.text =~ /There were no results matching the query./
              @log.warn "There were no results searching wikipedia for #{actor}"
              return true
            end
            false
          end

          def parse_results(search_results, actor)
            return [] unless search_results
            matches = []
            search_results.children.each do |element|
              link = element.at_css('a')
              confidence = estimate_result_confidence(element, actor)
              next if !link || !confident_enough?(confidence)
              matches << build_result_from(link, confidence, actor)
            end
            clean(matches)[0..SEARCH_RESULTS_MAXIMUM - 1]
          end

          def build_result_from(link, confidence, actor)
            @log.info "I think #{link['href']} may be a match for #{actor}"
            url = Enrichment::Search::Provider::Wikipedia.build_wiki_url(
              link.attr('href'))
            # TODO: Improve Redis cache layer / Code duplication Wiki::Base
            if CACHE.exists url && CACHE.get(url).to_s.length > 1000
              html = CACHE.get url
            else
              html = HTTP.get(url)
              if html.to_s.empty? && html['Location'] && html['Location'] == url
                @log.error "#{url} redirects to itself. Giving up on #{actor}"
                # TODO: is this clean?
                return nil
              else
                html = html.to_s
                CACHE.set url, html
              end
            end
            Enrichment::Search::Result.new(url, html, confidence, Enrichment::Search::Provider::Wikipedia)
          end

          def clean(matches)
            matches.compact.select { |r| confident_enough?(r.confidence) }
          end

          def confident_enough?(confidence)
            confidence > Enrichment::Search::Result::CONFIDENCE_PROBABLY_WRONG
          end

          def estimate_result_confidence(search_elem, actor)
            # TODO: Implement qualifies_as_disambig_match? neighbors? metrics?
            link = search_elem.at_css('a')
            if link && link['title'] =~ /List of/
              return Enrichment::Search::Result::CONFIDENCE_PROBABLY_WRONG
            end
            if mentions?(search_elem, @social_network.neighbors_of(actor)) ||
               in_description?(search_elem, actor)
              return Enrichment::Search::Result::CONFIDENCE_SOMEWHAT_SURE
            end
            Enrichment::Search::Result::CONFIDENCE_NOT_SURE
          end

          def mentions?(html_search_element, neighbors)
            neighbors.each do |neighbor|
              return true if html_search_element.text.match(neighbor.label)
            end
            false
          end

          def in_description?(_search_elem, _actor)
            false
          end
        end
      end
    end
  end
end
