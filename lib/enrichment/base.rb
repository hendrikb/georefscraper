# frozen_string_literal: true
module Enrichment
  # Class that manages  enrichment process for {SocialNetwork}'s
  # {SocialNetwork::Actor actors}
  class Base
    attr_reader :log

    def initialize(social_network)
      @log = Logging.logger[LogHelper.prefix(self.class.to_s)]

      if social_network.class != SocialNetwork::Base
        raise InvalidSocialNetworkError,
              "Need social network to enrich, got: #{social_network.class}"
      end
      @social_network = social_network
    end

    # Start enrichment process on @social_network
    # @return [void] Returns nothing, but the {SocialNetwork::Base} instance got
    #   enriched
    def enrich!
      log.info "Beginning enrichment on social network #{@social_network}"
      search_resources!
      scrape_resources!
      persist!
      geocode!
      log.info "Finished enrichment on social network #{@social_network}"
    end

    private

    def search_resources!
      CONFIG['enrichment']['search'].each do |search_provider, conf|
        provider = provider_instance_of(conf['provider_module'])
        @social_network.actors.each do |actor|
          log.info "[#{search_provider}] searching resources for #{actor}"
          actor.search_results << provider.search(actor)
        end
      end
    end

    def scrape_resources!
      #  TODO just a sketch
      stanford = Enrichment::Scrape::Provider::StanfordNLP::Base.new
      @social_network.actors.each do |actor|
        log.info "[#{stanford}] scraping relations for #{actor}"
        search_results = actor.search_results.flatten
        search_results.each do |result|
          actor.relations << stanford.scrape(result)
        end
      end
    end

    def entity_mentions_hash(er)
      {
        corefID: er.corefID,
        eend: er.eend,
        estart: er.estart,
        headPosition: er.headPosition,
        hend: er.hend,
        hstart: er.hstart,
        objectId: er.objectId,
        type: er.type.to_s,
        value: er.value.to_s,
        created_at: DateTime.now
      }
    end

    def persist!
      log.info 'Reached persistence phase'
      ds_sn = DB[:social_networks]
      social_network_id = ds_sn.insert(name: @social_network.name, created_at: DateTime.now)

      ds_actors = DB[:actors]
      @social_network.actors.each do |actor|
        actor_id = ds_actors.insert(node_id: actor.id, label: actor.label, class: actor.class.to_s.split(':').last, type: actor.type.to_s, social_network_id: social_network_id, created_at: DateTime.now)
        log.info "Persisted actor #{actor} with id #{actor_id}"

        actor.relations.flatten.each do |relation|
          ds_search_results = DB[:search_results]

          ds_em = DB[:entity_mentions]
          left_id = ds_em.insert(entity_mentions_hash(relation.left))
          right_id = ds_em.insert(entity_mentions_hash(relation.right))

          ds_relations = DB[:relations]

          result = relation.result
          result_id = ds_search_results.insert(resource: result.resource, confidence: result.confidence, html: result.html, search_provider_origin: result.search_provider_origin.name, actor_id: actor_id, created_at: DateTime.now)
          relation_id = ds_relations.insert(left: left_id, right: right_id, type: relation.type.to_s, search_result_id: result_id, actor_id: actor_id, created_at: DateTime.now)
          log.info "Persisted relation #{relation} with id #{relation_id}"
        end
      end

      ds_rels = DB[:relationships]
      @social_network.relationships.each do |rel|
        source = ds_actors.select(:id).where(node_id: rel.source.id).first
        target = ds_actors.select(:id).where(node_id: rel.target.id).first
        rel_id = ds_rels.insert(source: source[:id],
                                target: target[:id],
                                type: rel.type.to_s,
                                social_network_id: social_network_id,
                                created_at: DateTime.now)
        log.info "Persisted relationship #{rel} with id #{rel_id}"
      end
    end

    def geocode!
    end

    def provider_instance_of(provider_module_name)
      Object.const_get("#{provider_module_name}::Base").new(@social_network)
    end
  end

  # Raised if social network in {Base} is not of class {SocialNetwork::Base}
  class InvalidSocialNetworkError < StandardError; end
end
