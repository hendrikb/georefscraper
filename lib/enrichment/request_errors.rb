# frozen_string_literal: true
module Enrichment
  # Raised if a request returned with HTTP 30x status and a redirect is needed
  class RequestHttpRedirectNeededError < StandardError
    attr_reader :failure, :location
    # Instantize this class and show where stuff should be redirected
    # @param redirect_location [String] Tells people where to redirect to
    def initialize(redirect_location)
      @location = redirect_location
    end
  end

  # Raised if a request returned with HTTP 404 status and a redirect is needed
  class RequestHttpNotFoundError < StandardError
  end

  # Raised if a Web Request failed repeatedly and cannot be followed anymore
  class RequestPermanentlyFailedError < StandardError
    attr_reader :message
    # Initialize this error's class and provides a message what happened
    def initialize(message)
      @message = message
    end
  end
end
