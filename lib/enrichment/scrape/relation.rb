# frozen_string_literal: true
module Enrichment
  module Scrape
    # Represents contextual relation in a given text. Shows a triple of the form
    # (what ; relates_how ; to_what). Mainly used to ship outputs from the
    # {Enrichment::Scrape::Provider} environments, but can hold anything. See
    # the recommended file types in the initializer description, but basically
    # no file types are enforced - everything can be Object (or Hash, ...).
    class Relation
      attr_reader :left, :type, :right, :result
      # Create a contextual relation with these three parts in the tupel
      # described above
      # @param left [String] Subject part of this relation, String recommended
      # @param type [Symbol] Kind of relation, Symbol recommended.
      # @param right [String] Object part of this relation, String recommended
      # @param result [Enrichment::Search::Result] Former search result
      #   that represents a resource with a URL, a confidence estimation & html
      def initialize(left, type, right, result)
        @left = left
        @type = type
        @right = right
        @result = result
      end

      # Try to construct a human readable representation of this relation.
      def to_s
        "#{left} #{type.to_s.tr('_', ' ')} #{right} [stated in #{result.resource}]"
      end
    end
  end
end
