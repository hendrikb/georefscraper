# frozen_string_literal: true
require_relative './provider/stanford_nlp'

module Enrichment
  module Scrape
    # Module to namespace Scrape Provider classes
    module Provider
    end
  end
end
