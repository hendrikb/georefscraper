# frozen_string_literal: true
require 'facter'
require 'tempfile'
require 'nokogiri'
require 'uri'
require 'json'
require 'scalpel'
require 'stanford_corenlp_result'
require 'timeout'

module Enrichment
  module Scrape
    module Provider
      module StanfordNLP
        # Represents a timeout that gets thrown when the external Stanford
        # CoreNLP java application takes too long to process the input text
        class StanfordCoreNLPTimeout < Timeout::Error; end
        # Represents the a {Enrichment::Scrape::Provider} that implements an
        # interface for Stanford CoreNLP.
        class Base
          attr_reader :log
          # Default Timeout to wait for StanfordCoreNLP external java
          #   application to return before raising. The app sometimes hangs.
          NLP_DEFAULT_TIMEOUT = 15 * 60 # Seconds
          # Initializes the Stanford CoreNLP information extraction wrapper
          def initialize
            @log = Logging.logger[LogHelper.prefix(self.class.to_s)]
            begin
              @nlp_timeout = CONFIG['enrichment']['scrape']['StanfordCoreNLP']['TimeoutMinutes'] * 60
            rescue
              @nlp_timeout = NLP_DEFAULT_TIMEOUT
            end
          end

          # Should return a list of resources from a plain text document
          # @param search_result [Enrichment::Search::Result] Some result
          #   object from a former information retrieval process
          # @return {Enrichment::Scrape::Relation}} Representing a relation in
          #   the text
          def scrape(search_result)
            @search_result = search_result
            html_code = search_result.html
            url = search_result.resource

            html = Nokogiri::HTML(html_code)
            entity =  html.at('h1#firstHeading').inner_text
            content = html.at('div#mw-content-text')
            content.css('#toc').remove
            content.css('.thumb').remove
            content.css('.gallery').remove
            content.css('.infobox').remove

            content.xpath("//span[@id='External_links']/parent::h2/following-sibling::*").remove
            content.xpath("//span[@id='External_links']/parent::h2").remove
            content.xpath("//span[@id='References']/parent::h2/following-sibling::*").remove
            content.xpath("//span[@id='References']/parent::h2").remove
            content.xpath("//span[@id='Further_reading']/parent::h2/following-sibling::*").remove
            content.xpath("//span[@id='Further_reading']/parent::h2").remove

            content.xpath('//h1').remove
            content.xpath('//h2').remove
            content.xpath('//h3').remove
            content.xpath('//h4').remove
            content.xpath('//h5').remove
            content.xpath('//h6').remove

            plain_text = content.inner_text
            plain_text.gsub!(/\[[0-9]+\]/, '')
            plain_text.gsub!(/^(Book:|Main article:|See also|List of).*$/, '')
            plain_text.gsub!(/^[A-Z][a-z]+( [a-zA-Z]+[a-z])* portal/, '')
            plain_text.squeeze!("\n")

            max_sentences = 400
            sentence_list = Scalpel.cut(plain_text).reject { |s| s.split(' ').count < 4 }[0...max_sentences]
            @log.info("Scraping #{url} [#{entity}] with #{sentence_list.count} sentences ...")

            plain_text = sentence_list.join(' ')

            cache_key = "stanfordplaintext:#{url}"
            CACHE.set cache_key, plain_text

            cache_key = "stanfordnlpresult:#{url}"

            stanford_data_path = File.join('data', 'stanford_corenlp')
            # TODO: There is code duplication again with the cached corenlp
            # results
            corenlp_results = nil
            if CACHE.exists cache_key
              log.debug "#{cache_key} exists..."
              corenlp_results = CACHE.get cache_key
            else
              ram_usage = '10g'
              num_threads = Facter.value('processors')['count']

              plain_text_file = File.join(stanford_data_path, "#{Time.now.to_i}_#{url.gsub(/[^0-9A-Za-z.\-]/, '_')}")
              f = File.open(plain_text_file, 'w')
              f.write(plain_text)
              f.close

              cp = %w(stanford-corenlp-3.6.0.jar stanford-english-corenlp-2016-01-10-models.jar *)
                   .map { |p| "vendor/stanford-corenlp-full/#{p}" }.join(':')
              command = "java -Xmx#{ram_usage} -cp  #{cp} edu.stanford.nlp.pipeline.StanfordCoreNLP -props nlp_single_run.properties -file #{plain_text_file} -threads #{num_threads} -outputDirectory #{File.dirname(plain_text_file)}"
              begin
                log.debug "Running StanfordCoreNLP via: #{command}"

                begin
                  Timeout.timeout(@nlp_timeout, StanfordCoreNLPTimeout) do
                    `#{command}`
                  end
                rescue StanfordCoreNLPTimeout
                  log.error("Took more than #{@nlp_timeout} seconds to scrape #{search_results.resource} via \"#{command}\" - ABORTING. ")
                  return []
                end
                corenlp_result_file = "#{plain_text_file}.out"
                corenlp_results = File.read(corenlp_result_file)

                log.debug "StanfordNLP: Wrote plaintext to #{plain_text_file} and results to #{plain_text_file}.out"

                log.error 'CoreNLP results empty! That is very unlikely to happen!' if corenlp_results.to_s.empty?
                CACHE.set cache_key, corenlp_results
              rescue StandardError => e
                log.error e
                return []
              end
            end
            parser = StanfordCoreNLPResult::Base.new(corenlp_results)
            location_relations(parser)
          end

          private

          def makes_sense(relation)
            (relation.left.type == :PEOPLE || relation.left.type == :ORGANIZATION) &&
              (relation.right.type == :DATE || relation.right.type == :LOCATION) &&
              [:OrgBased_In, :Live_In, :Located_In].include?(relation.type)
          end

          def location_relations(parser)
            relations = []
            parser.relations.select { |r| makes_sense(r) }.each do |r|
              next unless r.right.type == :LOCATION || r.right.type == :DATE
              matching_location_ngrams = parser.ngrams
                                               .select { |n| n.contains_word?(r.right.value) }.each do |ngram|
                ngram.c.map do |monogram|
                  text_token = parser.tokens_from_word(monogram.token)
                  text_token.any? { |t| t.parameters.include?(:NamedEntityTag) && t.parameters[:NamedEntityTag] == :LOCATION }
                end
              end
              if matching_location_ngrams.count > 0
                matching_location_ngrams.each do |location_ngram|
                  right = StanfordCoreNLPResult::Relation::EntityMention.new(type: :DERIVED_NGRAM_LOCATION, value: location_ngram.to_s)
                  relations << Enrichment::Scrape::Relation.new(r.left, r.type, right, @search_result)
                end
              else
                relations << Enrichment::Scrape::Relation.new(r.left, r.type, r.right, @search_result)
              end
            end
            return [] if relations.count == 0
            # We need to guestimate, that the entity on the left side of the relation
            # with the MOST mentions, is that entity that the text is speaking about
            # so drop all other mentions.
            relations.uniq.sort_by { |r| r.left.value }
                     .group_by { |r| r.left.value }
                     .sort_by { |l| l[1].count }
                     .reverse.first.last
          end
        end
      end
    end
  end
end
