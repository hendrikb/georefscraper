# frozen_string_literal: true
require_relative './stanford_nlp/base'

module Enrichment
  module Scrape
    module Provider
      # StanfordNLP logics go here
      module StanfordNLP
      end
    end
  end
end
