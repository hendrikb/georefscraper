# frozen_string_literal: true
require_relative './scrape/provider'
require_relative './scrape/relation'

module Enrichment
  # Holds web page scraping logic
  module Scrape
  end
end
