# frozen_string_literal: true
require_relative './enrichment/base'
require_relative './enrichment/scrape'
require_relative './enrichment/search'
require_relative './enrichment/request_errors'

# Wrap around module for {SocialNetwork} enrichments  concerning
# {SocialNetwork::Actor actors}
module Enrichment; end
